package project1.service;

import java.util.List;

import project1.model.AuthenticatableAccount;
import project1.model.Reimbursement;
import project1.model.ReimbursementStatus;
import project1.model.UserAccount;

public interface AuthService 
{
	public UserAccount getAccount(AuthenticatableAccount credentials);
	
	public String getAccountDisplayStringById(int id);
}
