package project1.service;

import java.util.List;

import project1.model.Reimbursement;
import project1.model.ReimbursementRequest;
import project1.model.ReimbursementStatus;
import project1.model.UserAccount;

public interface ReimbursementService 
{
	//updates
	public boolean denyReimbursement(UserAccount account, String reimbursementID);
	public boolean approveReimbursement(UserAccount account, String reimbursementID);
	
	//selects
	public List<Reimbursement> getAllReimbursements();
	public List<Reimbursement> getAllReimbursementsByUser(UserAccount account);
	public List<Reimbursement> getAllReimbursementsByStatus(String status);
	
	//inserts
	public boolean addReimbursementRequest(UserAccount account, ReimbursementRequest reimbursementID);
}
