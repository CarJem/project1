package project1.service;

import java.util.List;

import project1.dao.AuthDao;
import project1.dao.AuthDaoImpl;
import project1.dao.ReimbursementDao;
import project1.dao.ReimbursementDaoImpl;
import project1.model.AuthenticatableAccount;
import project1.model.Reimbursement;
import project1.model.ReimbursementStatus;
import project1.model.UserAccount;

public class AuthServiceImpl implements AuthService 
{
	AuthDao dao = new AuthDaoImpl();

	@Override
	public UserAccount getAccount(AuthenticatableAccount credentials)
	{
		return dao.getUserAccount(credentials);
	}
	
	@Override
	public String getAccountDisplayStringById(int id) {
		return dao.getAccountDisplayStringById(id);
	}


}
