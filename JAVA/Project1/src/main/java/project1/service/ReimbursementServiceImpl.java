package project1.service;

import java.util.List;

import project1.dao.ReimbursementDao;
import project1.dao.ReimbursementDaoImpl;
import project1.model.Reimbursement;
import project1.model.ReimbursementRequest;
import project1.model.ReimbursementStatus;
import project1.model.UserAccount;

public class ReimbursementServiceImpl implements ReimbursementService 
{
	ReimbursementDao dao = new ReimbursementDaoImpl();

	@Override
	public boolean denyReimbursement(UserAccount account, String reimbursementID)
	{
		return dao.updateReimbursement(account, reimbursementID, ReimbursementStatus.Denied);
	}

	@Override
	public boolean approveReimbursement(UserAccount account, String reimbursementID)
	{
		return dao.updateReimbursement(account, reimbursementID, ReimbursementStatus.Approved);
	}

	@Override
	public List<Reimbursement> getAllReimbursements()
	{
		return dao.selectAllReimbursements();
	}

	@Override
	public List<Reimbursement> getAllReimbursementsByStatus(String status)
	{
		return dao.selectAllReimbursementsByStatus(status);
	}

	@Override
	public List<Reimbursement> getAllReimbursementsByUser(UserAccount account)
	{
		return dao.selectAllReimbursementsByUser(account);

	}

	@Override
	public boolean addReimbursementRequest(UserAccount account, ReimbursementRequest reimbursement)
	{
		return dao.insertReimbursementRequest(account, reimbursement);
	}



}
