package project1.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import project1.model.Reimbursement;
import project1.model.ReimbursementRequest;
import project1.model.ReimbursementStatus;
import project1.model.ReimbursementType;
import project1.model.UserAccount;

public class ReimbursementDaoImpl implements ReimbursementDao 
{

	@Override
	public boolean updateReimbursement(UserAccount account, String reimbursementID, ReimbursementStatus newStatus)
	{	
		var reimbursement = selectReimbursementsById(reimbursementID);
		if (reimbursement == null) return false;
		
		try (Connection conn = CustomConnectionFactory.getConnection()) 
		{	
			String sql = "UPDATE ers_reimbursement "
					+ "SET reimb_status_id = ?, reimb_resolved = current_timestamp, reimb_resolver = ?"
					+ "WHERE reimb_id = ?;";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, newStatus.getId());
			ps.setInt(2, account.getUserID());
			ps.setInt(3, reimbursement.getId());
			ps.executeUpdate();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public List<Reimbursement> selectAllReimbursements()
	{
		List<Reimbursement> reimbursements = new ArrayList<>();
		
		try (Connection conn = CustomConnectionFactory.getConnection()) 
		{
			String sql = createSelectStatement(null);

			PreparedStatement ps = conn.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			while(rs.next()) 
			{
				reimbursements.add(getReimbursement(rs));
			}
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return reimbursements;
	}
	
	@Override
	public List<Reimbursement> selectAllReimbursementsByUser(UserAccount account)
	{
		List<Reimbursement> reimbursements = new ArrayList<>();
		
		try (Connection conn = CustomConnectionFactory.getConnection()) 
		{
			String sql = createSelectStatement("WHERE reimb_author = ?");

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, account.getUserID());

			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) 
			{
				reimbursements.add(getReimbursement(rs));
			}
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return reimbursements;
	}
	
	private Reimbursement selectReimbursementsById(String string_id)
	{	
		try (Connection conn = CustomConnectionFactory.getConnection()) 
		{
			
			int id = Integer.parseInt(string_id);
			
			String sql = createSelectStatement("WHERE reimb_id = ?");

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) 
			{
				return getReimbursement(rs);
			}
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public List<Reimbursement> selectAllReimbursementsByStatus(String status)
	{
		List<Reimbursement> reimbursements = new ArrayList<>();
		
		try (Connection conn = CustomConnectionFactory.getConnection()) 
		{
			String sql = createSelectStatement("WHERE reimb_status = ?");

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, status);

			ResultSet rs = ps.executeQuery();

			while(rs.next()) 
			{
				reimbursements.add(getReimbursement(rs));
			}
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return reimbursements;
	}
	
	@Override
	public boolean insertReimbursementRequest(UserAccount account, ReimbursementRequest reimbursement)
	{
		try (Connection conn = CustomConnectionFactory.getConnection()) 
		{
			String sql = "SELECT COUNT(*) FROM ers_reimbursement;";
			
			PreparedStatement ps1 = conn.prepareStatement(sql);
			
			ResultSet rs = ps1.executeQuery();
			int count = 0;
			while (rs.next()) 
			{
				count = rs.getInt("count") + 1;
			}

			
			String sql2 = "INSERT INTO ers_reimbursement (reimb_id, reimb_amount, reimb_sumbitted, reimb_description, reimb_receipt, reimb_author, reimb_status_id, reimb_type_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?);";

			PreparedStatement ps2 = conn.prepareStatement(sql2);
			ps2.setInt(1, count + 1);
			ps2.setDouble(2, reimbursement.getAmount());
			ps2.setTimestamp(3, Timestamp.from(Instant.now()));
			ps2.setString(4, reimbursement.getDescription());
			ps2.setString(5, reimbursement.getReceipt());
			ps2.setInt(6, account.getUserID());
			ps2.setInt(7, ReimbursementStatus.Pending.getId());
			ps2.setInt(8, reimbursement.getType());

			ps2.executeUpdate();

		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			return false;
		}

		return true;
	}
		
	private String createSelectStatement(String extras)
	{
		String sql = "SELECT "
				+ "ers_reimbursement.reimb_id, "
				+ "ers_reimbursement.reimb_amount, "
				+ "ers_reimbursement.reimb_sumbitted, "
				+ "ers_reimbursement.reimb_resolved, "
				+ "ers_reimbursement.reimb_description, "
				+ "ers_reimbursement.reimb_receipt, "
				+ "ers_reimbursement.reimb_author, "
				+ "ers_reimbursement.reimb_resolver, "
				+ "ers_reimbursement.reimb_status_id, "
				+ "ers_reimbursement_status.reimb_status, "
				+ "ers_reimbursement.reimb_type_id, "
				+ "ers_reimbursement_type.reimb_type "
				+ "FROM ers_reimbursement "
				+ "JOIN ers_reimbursement_status "
				+ "ON ers_reimbursement.reimb_status_id=ers_reimbursement_status.reimb_status_id "
				+ "JOIN ers_reimbursement_type "
				+ "ON ers_reimbursement.reimb_type_id=ers_reimbursement_type.reimb_type_id "
				+ (extras == null ? ";" : extras);
		return sql;
	}
	
	private Reimbursement getReimbursement(ResultSet rs) throws SQLException 
	{
		
		var resolver_string = rs.getString("REIMB_RESOLVER");
		Integer resolver = (resolver_string == null ? null : Integer.parseInt(resolver_string));
		Timestamp resolved = rs.getTimestamp("REIMB_RESOLVED");
		
		return new Reimbursement(
				rs.getInt("REIMB_ID"),
				rs.getDouble("REIMB_AMOUNT"),
				rs.getTimestamp("REIMB_SUMBITTED"),
				resolved,
				rs.getString("REIMB_DESCRIPTION"),
				rs.getString("REIMB_RECEIPT"),
				rs.getInt("REIMB_AUTHOR"),
				resolver,
				new ReimbursementStatus(rs.getInt("REIMB_STATUS_ID"), rs.getString("REIMB_STATUS")),
				new ReimbursementType(rs.getInt("REIMB_TYPE_ID"), rs.getString("REIMB_TYPE"))
				
		);
	}

}