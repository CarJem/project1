package project1.dao;

import java.util.List;

import project1.model.Reimbursement;
import project1.model.ReimbursementRequest;
import project1.model.ReimbursementStatus;
import project1.model.UserAccount;

public interface ReimbursementDao {

	//inserts
	public boolean insertReimbursementRequest(UserAccount account, ReimbursementRequest request);
	
	//updates
	public boolean updateReimbursement(UserAccount account, String reimbursementID, ReimbursementStatus newStatus);
	
	//selects
	public List<Reimbursement> selectAllReimbursements();
	public List<Reimbursement> selectAllReimbursementsByUser(UserAccount account);
	public List<Reimbursement> selectAllReimbursementsByStatus(String status);
	
}
