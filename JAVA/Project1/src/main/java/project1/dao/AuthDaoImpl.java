package project1.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import project1.model.AuthenticatableAccount;
import project1.model.Reimbursement;
import project1.model.ReimbursementStatus;
import project1.model.ReimbursementType;
import project1.model.UserAccount;

public class AuthDaoImpl implements AuthDao 
{

	@Override
	public UserAccount getUserAccount(AuthenticatableAccount credentials)
	{
		UserAccount account = null;
		try (Connection conn = CustomConnectionFactory.getConnection()) 
		{
			String sql = "SELECT * FROM ers_users WHERE ers_username=? AND ers_password=?;";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, credentials.getUserName());
			ps.setString(2, credentials.getPassword());

			ResultSet rs = ps.executeQuery();

			while(rs.next()) 
			{
				account = new UserAccount(
						rs.getInt("ers_users_id"),
						rs.getString("ers_username"),
						rs.getString("ers_password"),
						rs.getString("user_first_name"),
						rs.getString("user_last_name"),
						rs.getString("user_email"),
						rs.getInt("user_role_id")
						);
			}
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return account;
	}
	
	@Override
	public String getAccountDisplayStringById(int id) 
	{
		String accountString = null;
		try (Connection conn = CustomConnectionFactory.getConnection()) 
		{
			String sql = "SELECT * FROM ers_users WHERE ers_users_id=?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();

			while(rs.next()) 
			{
				accountString = rs.getString("user_first_name") + " " + rs.getString("user_last_name");
			}
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return accountString;
	}

	
}