package project1.dao;

import java.util.List;

import project1.model.AuthenticatableAccount;
import project1.model.Reimbursement;
import project1.model.ReimbursementStatus;
import project1.model.UserAccount;

public interface AuthDao {

	public UserAccount getUserAccount(AuthenticatableAccount credentials);
	
	public String getAccountDisplayStringById(int id);

}
