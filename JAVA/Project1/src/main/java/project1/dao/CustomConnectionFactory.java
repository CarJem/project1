package project1.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class CustomConnectionFactory 
{
	public static String url = "jdbc:postgresql://34.122.4.214/project1";
	public static String username = "postgres";
	public static String password = "PpCYDgYC9hleyHwmU7pv";
	
	public static Connection getConnection() throws SQLException
	{
		return DriverManager.getConnection(url, username, password);
	}
}
