package project1;

import io.javalin.Javalin;
import project1.controller.FrontController;
import io.javalin.http.staticfiles.Location;

public class MainDriver 
{
	@SuppressWarnings("unused")
	public static void main(String[] args) 
	{
		Javalin app = Javalin.create(config -> {
			config.addStaticFiles(staticFiles -> {
				staticFiles.directory = "/resources";
				staticFiles.hostedPath = "/";
				staticFiles.location = Location.CLASSPATH;
			});
		}).start(9001);
		FrontController frontCont = new FrontController(app);
	}

}
