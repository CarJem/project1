package project1.model;

public class ReimbursementStatus 
{
	public static final ReimbursementStatus Pending = new ReimbursementStatus(0, "Pending");
	public static final ReimbursementStatus Approved = new ReimbursementStatus(1, "Approved");
	public static final ReimbursementStatus Denied = new ReimbursementStatus(2, "Denied");

	
	private int id;
	private String status;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public ReimbursementStatus(int id, String status) 
	{
        this.id = id;
        this.status = status;
    }
}
