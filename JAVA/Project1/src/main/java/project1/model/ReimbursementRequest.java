package project1.model;

import java.sql.Timestamp;

public class ReimbursementRequest 
{

	private int type;
	private String receipt;
	private String description;
	private double amount;
	
	public int getType()
	{
		return type;
	}

	public void setType(int type)
	{
		this.type = type;
	}

	public String getReceipt()
	{
		return receipt;
	}

	public void setReceipt(String receipt)
	{
		this.receipt = receipt;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public double getAmount()
	{
		return amount;
	}

	public void setAmount(double amount)
	{
		this.amount = amount;
	}
	
	public ReimbursementRequest() 
	{
		
	}

	public ReimbursementRequest(double amount, String description, String receipt, int type) 
	{
        this.amount = amount;
        this.description = description;
        this.receipt = receipt;
        this.type = type;
    }
}
