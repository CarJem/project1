package project1.model;

public class AuthenticatableAccount
{
	public String userName;
	public String password;
	
	public String getUserName()
	{
		return userName;
	}
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	public String getPassword()
	{
		return password;
	}
	public void setPassword(String password)
	{
		this.password = password;
	}
	
	public AuthenticatableAccount() 
	{
		
	}
	public AuthenticatableAccount(String userName, String password)
	{
		this.userName = userName;
		this.password = password;
	}
	
	
	
}
