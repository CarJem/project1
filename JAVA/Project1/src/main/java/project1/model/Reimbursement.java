package project1.model;

import java.sql.Timestamp;

public class Reimbursement 
{
	private int id;
	private ReimbursementType type;
	private ReimbursementStatus status;
	private Integer resolver;
	private int author;
	private String receipt;
	private String description;
	private Timestamp resolved;
	private Timestamp submitted;
	private double amount;
	
	public ReimbursementType getType()
	{
		return type;
	}

	public void setType(ReimbursementType type)
	{
		this.type = type;
	}

	public ReimbursementStatus getStatus()
	{
		return status;
	}

	public void setStatus(ReimbursementStatus status)
	{
		this.status = status;
	}
	
	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public Integer getResolver()
	{
		return resolver;
	}

	public void setResolver(Integer resolver)
	{
		this.resolver = resolver;
	}

	public int getAuthor()
	{
		return author;
	}

	public void setAuthor(int author)
	{
		this.author = author;
	}

	public String getReceipt()
	{
		return receipt;
	}

	public void setReceipt(String receipt)
	{
		this.receipt = receipt;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Timestamp getResolved()
	{
		return resolved;
	}

	public void setResolved(Timestamp resolved)
	{
		this.resolved = resolved;
	}

	public Timestamp getSubmitted()
	{
		return submitted;
	}

	public void setSubmitted(Timestamp submitted)
	{
		this.submitted = submitted;
	}

	public double getAmount()
	{
		return amount;
	}

	public void setAmount(double amount)
	{
		this.amount = amount;
	}
	
	public Reimbursement() 
	{
		
	}

	public Reimbursement(int id, double amount, Timestamp submitted, Timestamp resolved, String description, String receipt, int author, Integer resolver, ReimbursementStatus status, ReimbursementType type) 
	{
        this.id = id;
        this.amount = amount;
        this.submitted = submitted;
        this.resolved = resolved;
        this.description = description;
        this.receipt = receipt;
        this.author = author;
        this.resolver = resolver;
        this.status = status;
        this.type = type;
    }
}
