package project1.model;

public class ExResponseMessage 
{

	private String firstMessage;
	private String secondMessage;
	
	public ExResponseMessage() 
	{
	}

	public ExResponseMessage(String firstMessage, String secondMessage) 
	{
		super();
		this.firstMessage = firstMessage;
		this.secondMessage = secondMessage;
	}

	public String getFirstMessage() 
	{
		return firstMessage;
	}

	public void setFirstMessage(String firstMessage) 
	{
		this.firstMessage = firstMessage;
	}

	public String getSecondMessage() 
	{
		return secondMessage;
	}

	public void setSecondMessage(String secondMessage) 
	{
		this.secondMessage = secondMessage;
	}

	@Override
	public String toString() 
	{
		return "MyCustomResponseMessage [firstMessage=" + firstMessage + ", secondMessage=" + secondMessage + "]";
	}
}
