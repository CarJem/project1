package project1.controller;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;



import io.javalin.http.Context;
import project1.model.AuthenticatableAccount;
import project1.model.ExResponseMessage;
import project1.model.UserAccount;
import project1.service.AuthService;
import project1.service.AuthServiceImpl;

public class LoginAuthController 
{	
	private static AuthService service = new AuthServiceImpl();
	
	public static void login(Context context) 
	{	
		String userName = context.formParam("userName");
		String password = context.formParam("password");
		
		AuthenticatableAccount incomingAccount = new AuthenticatableAccount(userName, password);
		var resultingAccount = service.getAccount(incomingAccount);
		if (resultingAccount == null) 
		{
			context.json(new ExResponseMessage("Invalid Username / Password", "InvalidPassword"));
		}
		else 
		{
			context.sessionAttribute("currentUser", resultingAccount);
			context.json(new ExResponseMessage("Logged In", "LoggedIn"));
		}

		
	}
	
	public static void logout(Context context) 
	{
		context.sessionAttribute("currentUser", null);
		context.redirect("/html/login.html");
	}
	
	public static UserAccount getCurrentUserAccount(Context context) 
	{
		UserAccount currentUser = context.sessionAttribute("currentUser");
		if (currentUser != null) context.json(currentUser);
		else context.json(new ExResponseMessage("You are not currently logged in", "failure"));
		return currentUser;
	}
	
	public static void getLoginState(Context context) 
	{
		UserAccount currentUser = context.sessionAttribute("currentUser");
		if (currentUser != null) context.json(new ExResponseMessage("Logged In", "LoggedIn"));
		else context.json(new ExResponseMessage("Not Logged In", "NotLoggedIn"));
	}
	
	public static void getUserDisplayName(Context context) 
	{
		var id_ref = context.queryParam("id");
		if (id_ref.matches("null")) {
			context.json(new ExResponseMessage("N/A", ""));
		}
		else {
			Integer id = Integer.parseInt(id_ref);
			var result = service.getAccountDisplayStringById(id);
			context.json(new ExResponseMessage(result, ""));
		}

	}
	
}
