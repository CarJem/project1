package project1.controller;

import io.javalin.Javalin;

import static io.javalin.apibuilder.ApiBuilder.*; //this is where "path" came from

import java.nio.file.Path;


public class Dispatcher 
{

	public Dispatcher(Javalin app) 
	{
		init(app);
	}
	
	private static void init(Javalin app) 
	{
		app.routes(()-> 
		{
			path("/api", ()-> 
			{	
				get(LoginAuthController::getCurrentUserAccount);
				

				path("/status", ()->
				{
					get(LoginAuthController::getLoginState);
				});

				path("/login", ()->
				{
					post(LoginAuthController::login);
				});
				
				path("/logout", ()->
				{
					post(LoginAuthController::logout);
				});
				
				path("/search", ()-> 
				{
					path("/lookup", () ->
					{
						get(LoginAuthController::getUserDisplayName);
					});
				});
					
				path("/worker", ()-> 
				{
					get(LoginAuthController::getCurrentUserAccount);
					
					path("/tickets", () ->
					{
						get(ReimbursementController::listReimbursementsByUser);
					});
					
					path("/request", () ->
					{
						post(ReimbursementController::addReimbursementRequest);
					});
				});
				
				path("/manager", ()-> 
				{
					get(LoginAuthController::getCurrentUserAccount);
					
					path("/reimbursements", () ->
					{
						get(ReimbursementController::listReimbursements);
					});
					
					path("/approve", () ->
					{
						post(ReimbursementController::approveReimbursement);
					});
					path("/deny", () ->
					{
						post(ReimbursementController::denyReimbursement);
					});
				});
				

				

				
				
			});
			
		});
	}

	
}
