package project1.controller;

import io.javalin.Javalin;
import io.javalin.http.Context;
import io.javalin.http.ForbiddenResponse;
import io.javalin.http.UnauthorizedResponse;
import project1.model.ExResponseMessage;
import project1.model.Reimbursement;
import project1.model.UserAccount;

public class FrontController 
{

	Javalin app;
	Dispatcher dispatcher;
	
	public FrontController(Javalin app) 
	{
		this.app = app;
		
		app.before("/api/worker/*", FrontController::checkWorkerRequests);
		app.before("/api/manager/*", FrontController::checkManagerRequests);
		app.before("/api/search/*", FrontController::checkShearchRequests);
		
		this.dispatcher = new Dispatcher(app);
	}
	
	public static void checkShearchRequests(Context context) throws Exception
	{
		UserAccount account = LoginAuthController.getCurrentUserAccount(context);
		
		if (account == null) 
		{
			context.json(new ExResponseMessage("You are not currently logged in", "failure")).status(405);
			throw new ForbiddenResponse("You are not currently logged in");
		}
	}
	
	public static void checkManagerRequests(Context context) throws Exception
	{
		UserAccount account = LoginAuthController.getCurrentUserAccount(context);
		

		if (account == null) 
		{
			context.json(new ExResponseMessage("You are not currently logged in", "failure")).status(405);
			throw new ForbiddenResponse("You are not currently logged in");
		}
		else if (account.getRoleID() != 1) 
		{	
			context.json(new ExResponseMessage("Not Allowed","failure")).status(405);
			context.result("Account not allowed to use Manager Functions");
			throw new ForbiddenResponse("Account not allowed to use Manager Functions");
		}
	}
	
	public static void checkWorkerRequests(Context context) throws Exception
	{
		UserAccount account = LoginAuthController.getCurrentUserAccount(context);
		

		if (account == null) 
		{
			context.json(new ExResponseMessage("You are not currently logged in", "failure")).status(405);
			throw new ForbiddenResponse("Account not allowed to use Worker Functions");
		}
		else if (account.getRoleID() != 0) 
		{		
			context.json(new ExResponseMessage("Not Allowed","failure")).status(405);
			context.result("Account not allowed to use Manager Functions");
			throw new ForbiddenResponse("You are not currently logged in");
		}
	}
}
