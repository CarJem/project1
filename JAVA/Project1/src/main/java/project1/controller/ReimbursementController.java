package project1.controller;

import java.util.List;

import io.javalin.http.Context;
import project1.model.ExResponseMessage;
import project1.model.Reimbursement;
import project1.model.ReimbursementRequest;
import project1.service.ReimbursementService;
import project1.service.ReimbursementServiceImpl;

public class ReimbursementController 
{
	
	private static ReimbursementService service = new ReimbursementServiceImpl();

	public static void listReimbursements(Context context) 
	{
		String status = context.queryParam("status");
		List<Reimbursement> resultList;
		if (status.matches("all")) resultList = service.getAllReimbursements();
		else resultList = service.getAllReimbursementsByStatus(status);
		context.json(resultList);
	}
	
	public static void approveReimbursement(Context context) 
	{
		var account = LoginAuthController.getCurrentUserAccount(context);
		var data = context.queryParam("id");		
		var result = service.approveReimbursement(account, data);
		if (result) context.json(new ExResponseMessage("Successfully approved reimbursement","success")).status(201);
		else context.json(new ExResponseMessage("Failed to approve the specified reimbursement","failure")).status(400);
	}
	
	public static void denyReimbursement(Context context) 
	{
		var account = LoginAuthController.getCurrentUserAccount(context);
		var data = context.queryParam("id");	
		var result = service.denyReimbursement(account, data);
		if (result) context.json(new ExResponseMessage("Successfully denied reimbursement","success")).status(201);
		else context.json(new ExResponseMessage("Failed to deny the specified reimbursement","failure")).status(400);
	}
	
	public static void listReimbursementsByUser(Context context) 
	{
		var account = LoginAuthController.getCurrentUserAccount(context);
		var result = service.getAllReimbursementsByUser(account);
		context.json(result);
	}
	
	public static void addReimbursementRequest(Context context) 
	{
		var account = LoginAuthController.getCurrentUserAccount(context);
		var type = Integer.parseInt(context.formParam("type"));
		var receipt = context.formParam("receipt");
		var amount = Double.parseDouble(context.formParam("amount"));
		var description = context.formParam("description");
		
		ReimbursementRequest data = new ReimbursementRequest(amount, description, receipt, type);
		var result = service.addReimbursementRequest(account, data);
		context.json(result);
	}
}
