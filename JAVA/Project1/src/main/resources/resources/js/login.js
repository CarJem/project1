window.onload = function() { 
    init();
    validateLogin();
    setupLoginButton();
}


async function setupLoginButton() {
	document.forms['loginForm'].addEventListener('submit', (event) => {
    event.preventDefault();
    fetch(event.target.action, {
        method: 'POST',
        body: new URLSearchParams(new FormData(event.target))
    }).then((resp) => {
        return resp.json(); 
    }).then((body) => {
		let status = body.secondMessage;
		if (status == "InvalidPassword") 
		{
			let invalidTextRef = document.getElementById("invalidText");
			invalidTextRef.style.visibility = "visible";
		}
		else if (status == "LoggedIn") 
		{
			window.location.href="/html/mainpage.html";  
		}
    }).catch((error) => {
        // TODO handle error
    });
});
}

async function init() {
	let invalidTextRef = document.getElementById("invalidText");
	invalidTextRef.style.visibility = "hidden";
}

async function validateLogin()
{
    const responsePayload = await fetch("../api/status");
    const ourJSON = await responsePayload.json();
	let status = ourJSON.secondMessage;
	if (status == "LoggedIn") 
	{
		window.location.href="/html/mainpage.html";   
	}
}


























