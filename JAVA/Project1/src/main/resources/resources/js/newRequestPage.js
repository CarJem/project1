window.onload = function() { 
    validateLogin();
    setupRequestButton();
}


async function setupRequestButton() {
	document.forms['requestForm'].addEventListener('submit', (event) => {
    event.preventDefault();
    fetch(event.target.action, {
        method: 'POST',
        body: new URLSearchParams(new FormData(event.target))
    }).then((resp) => {
        return resp.json(); 
    }).then((body) => {
		console.log(body);
    }).catch((error) => {
        // TODO handle error
    });
});
}

async function validateLogin()
{
    const responsePayload = await fetch("../api/status");
    const ourJSON = await responsePayload.json();
	let status = ourJSON.secondMessage;
	if (status != "LoggedIn") 
	{
		window.location.href="/html/login.html";   
	}
}


























