
window.onload = function(){
    init();
}

async function init()
{
    const responsePayload = await fetch("../api/status");
    const ourJSON = await responsePayload.json();
	let status = ourJSON.secondMessage;
	if (status != "LoggedIn") 
	{
		window.location.href="/html/login.html";   
	}
	else 
	{
	    const responsePayload = await fetch("../api");
	    const accountData = await responsePayload.json();

		if (accountData.roleID == 0) 
		{
			document.getElementById("filterBtns").style.visibility = "hidden";
		    let response = await fetch("../api/worker/tickets");
			let json = await response.json();
			setTableData(json, accountData.userID);
		}
		else if (accountData.roleID == 1) 
		{	
			document.getElementById("requestButton").style.visibility = "hidden";
		    let response = await fetch("../api/manager/reimbursements?status=all");
			let json = await response.json();
			setTableData(json, accountData.userID);
		}
	    setupFilterButton();
	}
}

async function clearTableData() {
	let node = document.querySelector("#transactionTableBody");
	node.innerHTML = '';
}

async function denyRequest(id) {
    await fetch("../api/manager/deny?id=" + id, {
		 method: "POST"
	});
	clearTableData();
    let response = await fetch("../api/manager/reimbursements?status=all");
	let json = await response.json();
	setTableData(json, 0);
}

async function acceptRequest(id) {
    await fetch("../api/manager/approve?id=" + id, {
		 method: "POST"
	});
	clearTableData();
    let response = await fetch("../api/manager/reimbursements?status=all");
	let json = await response.json();
	setTableData(json, 0);
}

async function setTableData(json, userID) 
{

	for(let transaction of json) {
		
		let newTR = document.createElement("tr");
		let newTH = document.createElement("th");
		
		let newTD1 = document.createElement("td");
		let newTD2 = document.createElement("td");
		let newTD3 = document.createElement("td");
		let newTD4 = document.createElement("td");
		let newTD5 = document.createElement("td");
		let newTD6 = document.createElement("td");
		let newTD7 = document.createElement("td");
		let newTD8 = document.createElement("td");
		let newTD9 = document.createElement("td");
		let newTD10 = document.createElement("td");
		
		let newTD11 = document.createElement("td");
		let newTD12 = document.createElement("td");
		
		
		newTH.setAttribute("scope", "row");
			
	    let authorF = await fetch("../api/search/lookup?id=" + transaction.author);
	    let resolverF = await fetch("../api/search/lookup?id=" + transaction.resolver);
	    
	    let author = await authorF.json();
	    let resolver = await resolverF.json();
	    
	    let resolved = new Date(transaction.resolved);
		let submitted = new Date(transaction.submitted);
		
		let resolvedFinal = transaction.resolver == null ? "N/A" : resolved.toLocaleDateString("en-US")
		let submittedFinal = submitted.toLocaleDateString("en-US");
		
		let text1 = document.createTextNode(transaction.id);
		let text2 = document.createTextNode(transaction.type.type);
		let text3 = document.createTextNode(transaction.status.status);
		let text4 = document.createTextNode(resolver.firstMessage);
		let text5 = document.createTextNode(author.firstMessage);
		let text6 = document.createTextNode(transaction.receipt);
		let text7 = document.createTextNode(transaction.description);
		let text8 = document.createTextNode(resolvedFinal);
		let text9 = document.createTextNode(submittedFinal);
		let text10 = document.createTextNode(transaction.amount);
		
		
		newTD1.appendChild(text1);
		newTD2.appendChild(text2);
		newTD3.appendChild(text3);
		newTD4.appendChild(text4);
		newTD5.appendChild(text5);
		newTD6.appendChild(text6);
		newTD7.appendChild(text7);
		newTD8.appendChild(text8);
		newTD9.appendChild(text9);
		newTD10.appendChild(text10);
		
		if (userID == 0) 
		{
			let buttonAllow = document.createElement("button");
			buttonAllow.innerHTML = "&#10003;";
			buttonAllow.className = "btn btn-outline-success";
			buttonAllow.onclick = function()
			{
				acceptRequest(transaction.id);
			}
			
			let buttonDeny = document.createElement("button");
			buttonDeny.innerHTML = "&#10007;";
			buttonDeny.className = "btn btn-outline-danger";
			buttonDeny.onclick = function()
			{
				denyRequest(transaction.id);
			}
			
			newTD11.appendChild(buttonAllow);
			newTD12.appendChild(buttonDeny);
		}
		else {
			newTD11.appendChild(document.createTextNode(""));
			newTD12.appendChild(document.createTextNode(""));
		}
			
		newTR.appendChild(newTD1);
		newTR.appendChild(newTD2);
		newTR.appendChild(newTD3);
		newTR.appendChild(newTD4);
		newTR.appendChild(newTD5);
		newTR.appendChild(newTD6);
		newTR.appendChild(newTD7);
		newTR.appendChild(newTD8);
		newTR.appendChild(newTD9);
		newTR.appendChild(newTD10);
		newTR.appendChild(newTD11);
		newTR.appendChild(newTD12);
		

			
		let newSelection = document.querySelector("#transactionTableBody");
		newSelection.appendChild(newTR);
	}
	
}

async function filterAll() {
    let payLoad = await fetch("../api/manager/reimbursements?status=all");
	let body = await payLoad.json();
	clearTableData();
	setTableData(body, 0);
}

async function filterApproved() {
    let payLoad = await fetch("../api/manager/reimbursements?status=Approved");
	let body = await payLoad.json();
	clearTableData();
	setTableData(body, 0);
}

async function filterDenied() {
    let payLoad = await fetch("../api/manager/reimbursements?status=Denied");
	let body = await payLoad.json();
	clearTableData();
	setTableData(body, 0);
}

async function filterPending() {
    let payLoad = await fetch("../api/manager/reimbursements?status=Pending");
	let body = await payLoad.json();
	clearTableData();
	setTableData(body, 0);
}

async function setupFilterButton() {
	
	document.getElementById("allFilterBtn").addEventListener("click", filterAll);
	document.getElementById("pendingFilterBtn").addEventListener("click", filterPending);
	document.getElementById("approvedFilterBtn").addEventListener("click", filterApproved);
	document.getElementById("deniedFilterBtn").addEventListener("click", filterDenied);
}


