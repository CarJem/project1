CREATE TABLE ERS_REIMBURSEMENT_STATUS(
	REIMB_STATUS_ID INTEGER NOT NULL,
	REIMB_STATUS VARCHAR(10) NOT NULL,
		PRIMARY KEY(REIMB_STATUS_ID)
);

CREATE TABLE ERS_REIMBURSEMENT_TYPE(
	REIMB_TYPE_ID INTEGER NOT NULL,
	REIMB_TYPE VARCHAR(10) NOT NULL,
		PRIMARY KEY(REIMB_TYPE_ID)
);

CREATE TABLE ERS_USER_ROLES(
	ERS_USER_ROLE_ID INTEGER NOT NULL,
	USER_ROLE VARCHAR(10) NOT NULL,
		PRIMARY KEY(ERS_USER_ROLE_ID)
);

CREATE TABLE ERS_USERS(
	ERS_USERS_ID INTEGER NOT NULL,
	ERS_USERNAME VARCHAR(50) UNIQUE NOT NULL,
	ERS_PASSWORD VARCHAR(50) NOT NULL, --needs optional hashing
	USER_FIRST_NAME VARCHAR(100) NOT NULL,
	USER_LAST_NAME VARCHAR(100) NOT NULL,
	USER_EMAIL VARCHAR(150) UNIQUE NOT NULL,
	USER_ROLE_ID INTEGER NOT NULL,
		PRIMARY KEY(ERS_USERS_ID),
		CONSTRAINT USER_ROLES_FK FOREIGN KEY(USER_ROLE_ID) REFERENCES ERS_USER_ROLES(ERS_USER_ROLE_ID)
);

CREATE TABLE ERS_REIMBURSEMENT(
	REIMB_ID INTEGER NOT NULL,
	REIMB_AMOUNT INTEGER NOT NULL,
	REIMB_SUMBITTED TIMESTAMP NOT NULL,
	REIMB_RESOLVED TIMESTAMP,
	REIMB_DESCRIPTION VARCHAR(250),
	REIMB_RECEIPT VARCHAR(100),
	REIMB_AUTHOR INTEGER NOT NULL,
	REIMB_RESOLVER INTEGER,
	REIMB_STATUS_ID INTEGER NOT NULL,
	REIMB_TYPE_ID INTEGER NOT NULL,	
		PRIMARY KEY(REIMB_ID),
		CONSTRAINT ERS_USERS_FK_AUTH FOREIGN KEY(REIMB_AUTHOR) REFERENCES ERS_USERS(ERS_USERS_ID),
		CONSTRAINT ERS_USERS_FK_RESLVR FOREIGN KEY(REIMB_RESOLVER) REFERENCES ERS_USERS(ERS_USERS_ID),
		CONSTRAINT ERS_REIMBURSEMENT_STATUS_FK FOREIGN KEY(REIMB_STATUS_ID) REFERENCES ERS_REIMBURSEMENT_STATUS(REIMB_STATUS_ID),
		CONSTRAINT ERS_REIMBURSEMENT_TYPE_FK FOREIGN KEY(REIMB_TYPE_ID) REFERENCES ERS_REIMBURSEMENT_TYPE(REIMB_TYPE_ID)
);

-- User Roles
/*
 * 0 Employee
 * 1 Manager
 */
INSERT INTO ers_user_roles (ERS_USER_ROLE_ID, USER_ROLE) VALUES (0, 'Employee');
INSERT INTO ers_user_roles (ERS_USER_ROLE_ID, USER_ROLE) VALUES (1, 'Manager');

-- Reimbursement Statuses
/*
 * 0 Pending
 * 1 Approved
 * 2 Denied
 */
INSERT INTO ers_reimbursement_status (REIMB_STATUS_ID, REIMB_STATUS) VALUES (0, 'Pending');
INSERT INTO ers_reimbursement_status (REIMB_STATUS_ID, REIMB_STATUS) VALUES (1, 'Approved');
INSERT INTO ers_reimbursement_status (REIMB_STATUS_ID, REIMB_STATUS) VALUES (2, 'Denied');

-- Reimbursement Types
/*
 * 0 Lodging
 * 1 Food
 * 2 Travel
 * 3 Other
 */
INSERT INTO ers_reimbursement_type (REIMB_TYPE_ID, REIMB_TYPE) VALUES (0, 'Lodging');
INSERT INTO ers_reimbursement_type (REIMB_TYPE_ID, REIMB_TYPE) VALUES (1, 'Food');
INSERT INTO ers_reimbursement_type (REIMB_TYPE_ID, REIMB_TYPE) VALUES (2, 'Travel');
INSERT INTO ers_reimbursement_type (REIMB_TYPE_ID, REIMB_TYPE) VALUES (3, 'Other');

-- Users
INSERT INTO ERS_USERS (ERS_USERS_ID, ERS_USERNAME, ERS_PASSWORD, USER_FIRST_NAME, USER_LAST_NAME, USER_EMAIL, USER_ROLE_ID)
VALUES (0, 'manager', 'admin', 'Bob', 'Ernest', 'bob_ernest@gmail.com', 1);
INSERT INTO ERS_USERS (ERS_USERS_ID, ERS_USERNAME, ERS_PASSWORD, USER_FIRST_NAME, USER_LAST_NAME, USER_EMAIL, USER_ROLE_ID)
VALUES (1, 'worker', 'password', 'Carter', 'Wallace', 'carter_wallace@gmail.com', 0);
INSERT INTO ERS_USERS (ERS_USERS_ID, ERS_USERNAME, ERS_PASSWORD, USER_FIRST_NAME, USER_LAST_NAME, USER_EMAIL, USER_ROLE_ID)
VALUES (2, 'alex', 'alex', 'Alex', 'Freeman', 'freeman@gmail.com', 0);
INSERT INTO ERS_USERS (ERS_USERS_ID, ERS_USERNAME, ERS_PASSWORD, USER_FIRST_NAME, USER_LAST_NAME, USER_EMAIL, USER_ROLE_ID)
VALUES (3, 'tails', 'tails', 'Miles', 'Prowler', 'tailsFox@gmail.com', 0);


-- Get User (Login Test)
SELECT * 
FROM ers_users
WHERE ers_username='worker' 
AND ers_password='password';

-- Manager (Select Test)
SELECT 
	ers_reimbursement.REIMB_ID,
	ers_reimbursement.REIMB_AMOUNT,
	ers_reimbursement.REIMB_SUMBITTED,
	ers_reimbursement.REIMB_RESOLVED,
	ers_reimbursement.REIMB_DESCRIPTION,
	ers_reimbursement.REIMB_RECEIPT,
	ers_reimbursement.REIMB_AUTHOR,
	ers_reimbursement.REIMB_RESOLVER,
	ers_reimbursement.REIMB_STATUS_ID,
	ers_reimbursement_status.REIMB_STATUS,
	ers_reimbursement.REIMB_TYPE_ID,
	ers_reimbursement_type.REIMB_TYPE
FROM ers_reimbursement
JOIN ers_reimbursement_status 
	ON ers_reimbursement.reimb_status_id=ers_reimbursement_status.reimb_status_id
JOIN ers_reimbursement_type
	ON ers_reimbursement.reimb_type_id=ers_reimbursement_type.reimb_type_id;

-- Employee (Insert Test)
INSERT INTO ers_reimbursement (reimb_id, reimb_amount, reimb_sumbitted, reimb_description, reimb_receipt, reimb_author, reimb_status_id, reimb_type_id) 
VALUES (1, 1000, current_timestamp, 'Details', 'Details2', 1, 1, 1);

-- Employee (Select Test)
SELECT 
	ers_reimbursement.REIMB_ID,
	ers_reimbursement.REIMB_AMOUNT,
	ers_reimbursement.REIMB_SUMBITTED,
	ers_reimbursement.REIMB_RESOLVED,
	ers_reimbursement.REIMB_DESCRIPTION,
	ers_reimbursement.REIMB_RECEIPT,
	ers_reimbursement.REIMB_AUTHOR,
	ers_reimbursement.REIMB_RESOLVER,
	ers_reimbursement.REIMB_STATUS_ID,
	ers_reimbursement_status.REIMB_STATUS,
	ers_reimbursement.REIMB_TYPE_ID,
	ers_reimbursement_type.REIMB_TYPE
FROM ers_reimbursement
JOIN ers_reimbursement_status 
	ON ers_reimbursement.reimb_status_id=ers_reimbursement_status.reimb_status_id
JOIN ers_reimbursement_type
	ON ers_reimbursement.reimb_status_id=ers_reimbursement_type.reimb_type_id
WHERE reimb_author=1;

-- Manager (Select By Status Test)
SELECT 
	ers_reimbursement.REIMB_ID,
	ers_reimbursement.REIMB_AMOUNT,
	ers_reimbursement.REIMB_SUMBITTED,
	ers_reimbursement.REIMB_RESOLVED,
	ers_reimbursement.REIMB_DESCRIPTION,
	ers_reimbursement.REIMB_RECEIPT,
	ers_reimbursement.REIMB_AUTHOR,
	ers_reimbursement.REIMB_RESOLVER,
	ers_reimbursement.REIMB_STATUS_ID,
	ers_reimbursement_status.REIMB_STATUS,
	ers_reimbursement.REIMB_TYPE_ID,
	ers_reimbursement_type.REIMB_TYPE
FROM ers_reimbursement
JOIN ers_reimbursement_status 
	ON ers_reimbursement.reimb_status_id=ers_reimbursement_status.reimb_status_id
JOIN ers_reimbursement_type
	ON ers_reimbursement.reimb_status_id=ers_reimbursement_type.reimb_type_id
WHERE ers_reimbursement.REIMB_TYPE_ID=1;

-- Manager (Approve Test)
UPDATE ers_reimbursement 
SET reimb_status_id = 1, reimb_resolved = current_timestamp, reimb_resolver = 0
WHERE reimb_id = 1;

SELECT COUNT(*)
FROM ers_reimbursement;



ALTER TABLE ers_reimbursement ALTER COLUMN REIMB_RESOLVER SET NOT NULL;
ALTER TABLE ers_reimbursement ALTER COLUMN REIMB_RESOLVED SET NOT NULL;

	